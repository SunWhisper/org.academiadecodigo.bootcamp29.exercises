package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public abstract class Car {

    /** The position of the car on the grid */
    private Position pos;
    private boolean crashed;

    public Car() {
        this.pos = new Position();
        crashed = false;
    }

    public Position getPos() {
        return pos;
    }

    public void setPos(Position pos) {
        this.pos = pos;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public abstract void move();

    public  void checkCrash(Car c){
        if (c.getPos().getCol() == this.pos.getCol() && c.getPos().getRow() == this.pos.getRow())
            crashed = true;
    }
}
