package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class Fiat extends Car {


    public Fiat() {
        super();
    }


    @Override
    public void move() {
        Position pos;
        int col;
        int row;

        if (Math.random()<0.5) { //col moves

            pos=super.getPos();
            col=pos.getCol();

            if(col > 0 && col < (Field.getWidth()-1)) {

                if (Math.random() < 0.5) {
                    col = (pos.getCol() + 1);

                    pos.setCol(col);
                    return;
                }
                col = (pos.getCol() - 1);

                pos.setCol(col);
                return;
            }

            if(col <= 0) {
                col = (pos.getCol() + 1);

                pos.setCol(col);
                return;
            }

            col = (pos.getCol()-1);

            pos.setCol(col);
            return;
        }

        pos = super.getPos();
        row = pos.getRow();

        if(row > 0 && row < (Field.getHeight()-1)) { //row moves

            if (Math.random() < 0.5) {
                row = (pos.getRow() + 1);

                pos.setRow(row);
                return;
            }
            row = (pos.getRow() - 1);

            pos.setRow(row);
            return;
        }
        if(row <= 0) {
            row = (pos.getRow() + 1);

            pos.setRow(row);
            return;
        }

        row = (pos.getRow()-1);

        pos.setRow(row);
        return;
    }

    @Override
    public String toString() {
        if(!isCrashed()) {
            return "F";
        }
        return "C";
    }


}
