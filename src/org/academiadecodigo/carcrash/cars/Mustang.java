package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class Mustang extends Car {

    public Mustang() {
        super();
    }


    @Override
    public void move() {
        Position pos;
        int col;
        int row;

        if (Math.random()<0.5) { //col moves

            pos=super.getPos();
            col=pos.getCol();

            if(col > 1 && col < (Field.getWidth()-1)) {

                if (Math.random() < 0.5) {
                    if ((col + 2) < (Field.getWidth()-1)) {
                        col = (col + 2);

                        pos.setCol(col);
                        return;
                    }

                    col = (col + 1);

                    pos.setCol(col);
                    return;
                }
                col = (col - 2);

                pos.setCol(col);
                return;
            }

            if(col <= 0) {
                col = (col + 2);

                pos.setCol(col);
                return;
            }
            if ((col - 2) >= 0) {
                col = (col - 2);

                pos.setCol(col);
                return;
            }

            col = (col - 1);

            pos.setCol(col);
            return;

        }

        pos = super.getPos();
        row = pos.getRow();

        if(row > 1 && row < (Field.getHeight()-1)) { //row moves

            if (Math.random() < 0.5) {
                if (row < (Field.getHeight()-1)) {
                    row = (row + 2);

                    pos.setRow(row);
                    return;
                }

                row = (row + 1);

                pos.setRow(row);
                return;
            }
            row = (row - 2);

            pos.setRow(row);
            return;
        }
        if(row <= 0) {

            row = (row + 2);

            pos.setRow(row);
            return;
        }
        if ((row - 2) >= 0) {
            row = (row - 2);

            pos.setRow(row);
            return;
        }

        row = (row - 1);

        pos.setRow(row);
        return;

    }

    @Override
    public String toString() {
        if(!isCrashed()) {
            return "M";
        }
        return "C";
    }
}
