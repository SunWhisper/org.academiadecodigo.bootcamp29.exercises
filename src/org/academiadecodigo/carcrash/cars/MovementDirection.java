package org.academiadecodigo.carcrash.cars;

public enum MovementDirection {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
