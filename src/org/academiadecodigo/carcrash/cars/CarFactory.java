package org.academiadecodigo.carcrash.cars;


public class CarFactory {

    public static  Car getNewCar() {

        CarType currentCar;
        Car car = null;

        if (Math.random()<= 0.50) {
            currentCar = CarType.FIAT;
        }else {
            currentCar = CarType.MUSTANG;
        }

        switch(currentCar){

               case FIAT:

                   car = new Fiat();

                   break;

               case MUSTANG:

                   car = new Mustang();

                   break;

           }

        return car;
    }
}
